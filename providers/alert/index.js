'use strict'
const telegram = use('Telegram')
var CronJob = require('node-cron')

class Alert {
    constructor(Config) {
        this.Config = Config
        this.batch = new Array()
        this.alarm = false
        this.cron = new Array
        this.message = 'Message not set'
    }

    setup() {
        this.cron.push(CronJob.schedule('*/2 * * * *', () => {
            if(this.alarm){
                telegram.sendMessage(this.message)
            }
        }, {
            scheduled: true
        })
        )
        return true
    }

    push(param1, condition, param2, message = 'Message not set') {
        if (eval(`${param1} ${condition} ${param2}`)) {
            this.message = message
            this.alarm = true
        } else {
            this.alarm = false
        }
        return this.alarm
    }

}

module.exports = Alert
