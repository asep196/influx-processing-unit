'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class AlertProvider extends ServiceProvider {

  register() {
    this.app.singleton('Alert', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('.'))(Config)
    })
  }

  boot() {
    this.app.use('Alert').setup()
  }
}

module.exports = AlertProvider
