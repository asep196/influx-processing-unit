'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class InfluxProvider extends ServiceProvider {

  register() {
    this.app.singleton('InfluxDB', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('.'))(Config)
    })
  }

  boot() {
    this.app.use('InfluxDB').connect()
    this.app.use('InfluxDB').setWriteApi()
    this.app.use('InfluxDB').setQuery()
  }
}

module.exports = InfluxProvider
