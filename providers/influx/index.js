'use strict'
const Config = use('Config')
const { InfluxDB, Point } = require('@influxdata/influxdb-client')

class Influx {
    constructor(Config) {
        this.Config = Config
        this.influxDB = null
        this.queryApi = null
        this.org = null
        this.writeApi = null
        this.bucket = null
    }
    connect() {
        this.influxDB = new InfluxDB({ url: Config.get('database.influxdb.connection.host'), token: Config.get('database.influxdb.connection.token') })
        return this.influxDB
    }
    setQuery(org = this.org) {
        this.queryApi = this.influxDB.getQueryApi(org)
    }
    async query(query, callback) {
        let results = new Array()
        const fluxObserver = {
            next(row, tableMeta) {
                const o = tableMeta.toObject(row)
                results.push(o._value)
            },
            error(error) {
                console.error(error)
                console.log('\nFinished ERROR')
            },
            complete() {
                callback(results)
            }
        }
        this.queryApi.queryRows(query, fluxObserver)
    }
    setWriteApi(bucket = Config.get('database.influxdb.connection.bucket'), org = Config.get('database.influxdb.connection.org'), timestamp = 'ns') {
        this.org = org
        this.bucket = bucket
        this.writeApi = this.influxDB.getWriteApi(this.org, bucket, timestamp)
    }
    write(name, measurements) {
        let lineProtocol = measurements.map(function (e) {
            return new Point(name)
                .tag('device', e.tags.device)
                .floatField('temp', parseFloat(e.fields.temp))
                .timestamp(new Date()).toLineProtocol()
        })
        this.writeApi.writeRecords(lineProtocol)
        this.writeApi.flush()
    }
}

module.exports = Influx
