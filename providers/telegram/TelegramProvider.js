'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class TelegramProvider extends ServiceProvider {
  
  register () {
    this.app.singleton('Telegram', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('.'))(Config)
    })
  }

  boot () {
    this.app.use('Telegram').setBot()
    this.app.use('Telegram').spawn()
  }
}

module.exports = TelegramProvider
