'use strict'
const { Telegraf } = require('telegraf')
const influxDB = use('InfluxDB')
const Config = use('Config')
const Wemos = use('Wemos')

class Telegram {
    constructor(Config) {
        this.Config = Config
        this.bot = null
        this.chatID = null
    }

    setBot() {
        this.bot = new Telegraf(Config.get('auth.telegram.token'))
    }

    sendMessage(message) {
        if(this.chatID==null){
            return false
        }
        this.bot.telegram.sendMessage(this.chatID, message)
    }

    spawn() {

        this.bot.command('temperature', (ctx) => {
            influxDB.query('from(bucket:"lattice") |> range(start: -3m, stop: now())', function (e) {
                let avg, predictor = e.length
                let sum = e.reduce(function (total, i) {
                    return total + i
                })
                avg = (sum / predictor).toFixed(2)
                ctx.telegram.sendMessage(ctx.message.chat.id, `Suhu ${avg}°C`)
            })
        })
        this.bot.command('start', (ctx) => {
            this.chatID = ctx.message.chat.id
            ctx.telegram.sendMessage(ctx.message.chat.id, 'Server started')
        })
        this.bot.command('alarm', (ctx) => {
            let message=`Status alarm ${Wemos.status}\n Hidupkan alarm /alarm_on`
            if(Wemos.status!='off'){
                message = `Status alarm ${Wemos.status}\n Matikan alarm /alarm_off`
            }
            ctx.telegram.sendMessage(ctx.message.chat.id, message)
        })
        this.bot.command('alarm_on', (ctx) => {
            Wemos.on()
            ctx.telegram.sendMessage(ctx.message.chat.id, `Sukses, cek /alarm`)
        })
        this.bot.command('alarm_off', (ctx) => {
            Wemos.off()
            ctx.telegram.sendMessage(ctx.message.chat.id, `Sukses, cek /alarm`)
        })
        this.bot.on('text', (ctx) => {
            this.chatID = ctx.message.chat.id
            ctx.telegram.sendMessage(ctx.message.chat.id, `Menu :\n1. Start services Telegram\n2. Cek temperature /temperature\n3. Cek alarm /alarm`)
        })

        this.bot.launch()

        // Enable graceful stop
        process.once('SIGINT', () => bot.stop('SIGINT'))
        process.once('SIGTERM', () => bot.stop('SIGTERM'))
    }

}

module.exports = Telegram
