'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class WemosProvider extends ServiceProvider {

  register() {
    this.app.singleton('Wemos', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('.'))(Config)
    })
  }

  boot() {
    this.app.use('Wemos').start()
  }
}

module.exports = WemosProvider
