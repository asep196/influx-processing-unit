'use strict'
const emqx = use('Emqx')

class Wemos {
    constructor(Config) {
        this.Config = Config
        this.status = "off"
        this.challenge = 0
        this.pair = 0
        this.timeout = null
        this.waiting = false
        this.action = null
    }
    /**
     * Wemos harus mengirimkan payload ack dan challenge dalam bentuk JSON ke inTopic
      contoh : {"ack": 42442, "challenge": 42437}
     */
    start() {
        emqx.client.subscribe('inTopic')
        emqx.client.on('message', (topic, message, packet) => {
            switch (topic) {
                case 'inTopic':
                    try {
                        let temp = JSON.parse(message.toString())
                        if (temp.challenge == this.challenge && temp.ack == this.pair) {
                            this.status = this.action
                            this.challenge = 0
                            this.pair = 0
                            clearTimeout(this.timeout)
                            this.waiting = false
                            this.action = null
                        }
                    }
                    catch (e) {
                    }
                    break;
                default:
            }
        })
    }

    on() {
        if (!this.waiting) {
            this.challenge = this.setChallenge()
            this.pair = this.setPair(this.challenge)
            this.generateNumber = true
            this.timeout = setTimeout(function () {
                this.waiting = false
                this.challenge = 0
                this.pair = 0
                this.timeout = null
                this.action = null
            }, 10000)
            emqx.publish('inTopic', `{"buzzer":"on","challenge":${this.challenge}}`)
            this.action = 'on'
            this.waiting = true
        }
    }

    off() {
        if (!this.waiting) {
            this.challenge = this.setChallenge()
            this.pair = this.setPair(this.challenge)
            this.generateNumber = true
            this.timeout = setTimeout(function () {
                this.waiting = false
                this.challenge = 0
                this.pair = 0
                this.timeout = null
                this.action = null
            }, 10000)
            emqx.publish('inTopic', `{"buzzer":"off","challenge":${this.challenge}}`)
            this.action = 'off'
            this.waiting = true
        }
    }

    setChallenge() {
        return Math.floor(10000 + Math.random() * 90000)
    }

    setPair(challenge) {
        return (challenge + 5)
    }


}

module.exports = Wemos
