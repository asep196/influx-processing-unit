'use strict'
const mqtt = require('mqtt')
const Config = use('Config')

class Emqx {
    constructor(Config) {
        this.Config = Config
        this.client = null
    }

    connect() {
        this.client = mqtt.connect({
            host: Config.get('auth.emqx.host'),
            port: Config.get('auth.emqx.port'),
            username: Config.get('auth.emqx.username'),
            password: Config.get('auth.emqx.password'),
            clean: true
        })
    }
    publish(topic, message) {
        let status = 'fail'
        this.client.publish(topic, message, {
            qos: 0,
            retain: false,
            dup: false
        }, function (err) {
            if (!err) {
                status = 'success'
            }
        })
        return status
    }

}

module.exports = Emqx
