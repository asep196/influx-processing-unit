'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class EmqxProvider extends ServiceProvider {

  register() {
    this.app.singleton('Emqx', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('.'))(Config)
    })
  }

  boot() {
    this.app.use('Emqx').connect()
  }
}

module.exports = EmqxProvider
