'use strict'

const emqx = use('Emqx')
const { test } = use('Test/Suite')('Push Data to Emqx')

test('Create connection', async ({ assert }) => {
  assert.isObject(emqx.client)
})
test('Push data to Emqx', async ({ assert }) => {
  let publish = emqx.publish('ujicoba', '{"buzzer":"off"}')
  assert.equal(publish, 'success')
})
