'use strict'

const influx = use('InfluxDB')
const { test } = use('Test/Suite')('Push Data to Influx')

test('Create connection', async ({ assert }) => {
  assert.hasAllKeys(influx.influxDB, [
    '_options', 'transport', 'processCSVResponse'
  ])
})
test('Set writeAPI', async ({ assert }) => {
  assert.hasAllKeys(influx.queryApi, [
    'transport', 'createCSVResponse', 'options'
  ])
})
// todo: Add dummy data
test('Write data to Influx', async ({ assert }) => {
  assert.hasAllKeys(influx.influxDB, [
    '_options', 'transport', 'processCSVResponse'
  ])
})
