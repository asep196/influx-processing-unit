'use strict'
const InfluxDB = use('InfluxDB')
const alert = use('Alert')

class InfluxController {
    async create({ request, response }) {
        if (typeof request.input('metrics') == 'object') {
            let measurement = request.input('metrics')
            InfluxDB.write('temperature', measurement)
            let avg, sum = 0, predictor = measurement.length
            measurement.map((e) => {
                sum += e.fields.temp
            })
            avg = (sum / predictor).toFixed(2)
            alert.push(avg, '>', 27, 'Hareudang!!!')
            return response.status(202).send({ status: 'accepted' })
        } else {
            return response.status(400).send({ status: 'bad request' })
        }
    }
}

module.exports = InfluxController
